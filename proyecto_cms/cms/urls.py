from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('acceso', views.loggedIn),
    path('logout', views.logout_vista),
    path('<str:recurso>/', views.get_resources),
    path('<str:recurso>', views.get_resources),
]