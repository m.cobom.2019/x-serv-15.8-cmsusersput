from django.shortcuts import render

from django.http import HttpResponse, Http404

from django.views.decorators.csrf import csrf_exempt

from .models import Contenido

from django.template import loader #permite manejar el template

from django.contrib.auth import logout

from django.shortcuts import redirect

formulario = """
<form action="" method="POST">
    valor:<input type="text" name="valor">
    </br><input type="submit" values="Enviar">
</form>

"""
def index(request):
    # 1 Obtener el contenido a tratar en el template
    content_list = Contenido.objects.all()

    # 2 Cargar el template que vamos a usar
    template = loader.get_template('cms/index.html')

    # 3 Crear el contexto que se enviara al template
    contexto = {
        'contenido_lista': content_list
    }

    # 4 Renderizar el template y responder
    return HttpResponse(template.render(contexto, request))


# Create your views here.
@csrf_exempt#Decorador para quitar el control de seguridad de CSRF
def get_resources(request, recurso):

    #PUT
    if request.method in ["PUT","POST"]:
        if request.method == "PUT":
            Valor = request.body.decode()
        else:
            Valor = request.POST["valor"]

        #Gestiono los datos en la base de datos
        try:
            contenido = Contenido.objects.get(clave=recurso)
            contenido.valor = Valor
            contenido.save()
        except Contenido.DoesNotExist:
            c = Contenido(clave=recurso, valor=Valor)
            c.save()


    #Metodos GET
    try:
        contenido = Contenido.objects.get(clave=recurso)
        respuesta = HttpResponse("El recurso pedido es: "+ contenido.clave+ ", cuyo valor es: "+contenido.valor+", y su identificador es : "+str(contenido.id))
    except Contenido.DoesNotExist:
        if request.user.is_authenticated:
            respuesta = HttpResponse(formulario)
        else:
            respuesta = HttpResponse("No estas autenticado, <a href=/login>autenticate!</a>")

    return respuesta


def loggedIn(request):
    if request.user.is_authenticated:
        respuesta = "El usuario "+ request.user.username+ " esta autenticado"

    else:
        respuesta = "No estas logeado, entra en el <a href=/login" \
                    ">login</a>"

    return HttpResponse(respuesta)

def logout_vista(request):
    logout(request)
    return redirect("/cms/")

